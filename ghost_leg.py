import math

"""
Could we implement ghost_leg with reduce?
    reduce(lambda x, y: x+y, [1, 2, 3, 4, 5]) 
    calculates ((((1+2)+3)+4)+5)
"""

def ghost_leg(elements, legs):
    permutation = list(elements)
    for leg in legs:
        if abs(leg[0] - leg[1]) != 1:
            raise ValueError("Legs must connect adjacent verticals")
        permutation[leg[0]], permutation[leg[1]] = permutation[leg[1]], permutation[leg[0]]

    return permutation

def count_periodicity(example_items, example_legs):
    count = 1
    current_state = ghost_leg(example_items,example_legs)
    while current_state != example_items:
        current_state = ghost_leg(current_state,example_legs)
        count += 1

        assert count < math.factorial(len(example_items))
            

    return count
