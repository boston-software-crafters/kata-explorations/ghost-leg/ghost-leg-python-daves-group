from ghost_leg import ghost_leg
from ghost_leg import count_periodicity
import pytest

example_items = ['A', 'B', 'C', 'D']

example_legs = [ (0, 1), (2, 3), (1, 2), (2, 3), (1, 2), (0, 1) ]

example_output = ['C', 'B', 'D', 'A']


def test_empty_ghost_leg():

    perm = ghost_leg((), ())
    assert len(perm) == 0

def test_no_legs_gives_same_list_back():

    perm = ghost_leg(example_items, ())
    assert perm == example_items

def test_invalid_legs():
    with pytest.raises(ValueError):
        ghost_leg(example_items, ((0, 0), ) )

def test_non_adjacent_legs():
    with pytest.raises(ValueError):
        ghost_leg(example_items, ((0, 2), ) )

def test_adjacent_legs_a_b():
    perm = ghost_leg(example_items, ((0, 1), ) )
    assert perm[0:2] == ['B', 'A']

def test_adjacent_legs_b_c():
    perm = ghost_leg(example_items[1:], ((0, 1), ) )
    assert perm[0:2] == ['C', 'B']

def test_reversed_legs():
    perm = ghost_leg(example_items, ((0, 1), (0, 1)) )
    assert perm[0:2] == ['A', 'B']

def test_example_ghost_leg():
    perm = ghost_leg(example_items, example_legs)
    assert perm == example_output

def test_perodicity_three_example():
    perm = ghost_leg(example_items, example_legs)
    perm = ghost_leg(perm, example_legs)
    perm = ghost_leg(perm, example_legs)
    assert perm == example_items

def test_periodicity_counter():
    assert count_periodicity(example_items, example_legs) == 3



# Mob Order:
#   - Vlad  [D]
#   - Dave  
#   - Ali   
#   - JF    [N]

# Output is the same length as input

# Output contains the same elements as input (in different order)

# Ghost Legs have a finite periodicity
# (Repeated permutations will eventually return to the original order)
